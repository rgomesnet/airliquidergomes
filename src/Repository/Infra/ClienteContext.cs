﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Repository.Infra.Mappings;

namespace Repository.Infra
{
    public class ClienteContext : DbContext
    {
        public ClienteContext() { }

        public ClienteContext(DbContextOptions<ClienteContext> options) : base(options) { }

        public DbSet<Cliente> Clientes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ClienteMap());
        }
    }
}
