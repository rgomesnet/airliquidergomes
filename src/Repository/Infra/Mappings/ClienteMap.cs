﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Repository.Infra.Mappings
{
    public class ClienteMap : IEntityTypeConfiguration<Cliente>
    {
        public void Configure(EntityTypeBuilder<Cliente> builder)
        {
            builder.HasKey(o => o.Id);

            builder.Property(o => o.Nome)
                   .HasMaxLength(200)
                   .IsRequired();

            builder.Property(o => o.Idade)
                   .HasMaxLength(200)
                   .IsRequired();
        }
    }
}