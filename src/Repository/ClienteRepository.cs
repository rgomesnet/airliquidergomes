﻿using System;
using System.Threading.Tasks;
using Repository.Infra;
using Domain.Models;
using Domain.Repositories;
using Domain.Services.DTO;

namespace Repository
{
    public class ClienteRepository : IClienteRepository
    {
        private readonly ClienteContext context;
        public ClienteRepository(ClienteContext context)
        {
            this.context = context;
        }

        public async Task<Cliente> Obter(Guid clienteId)
        {
            var cliente = await this.context.Clientes.FindAsync(clienteId);
            return cliente;
        }

        public async Task<Cliente> Criar(Cliente novoCliente)
        {
            await this.context.Clientes.AddAsync(novoCliente);
            await this.context.SaveChangesAsync();

            return novoCliente;
        }

        public async Task<Cliente> Atualizar(Cliente cliente)
        {
            this.context.Clientes.Update(cliente);
            await this.context.SaveChangesAsync();

            return cliente;
        }

        public async Task Excluir(Cliente cliente)
        {
            this.context.Clientes.Remove(cliente);
            await this.context.SaveChangesAsync();
        }
    }
}
