﻿using System;

namespace Domain.Models
{
    public class Cliente
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public int Idade { get; set; }

        public override bool Equals(object obj)
        {
            var outroCliente = obj as Cliente;

            return this.Nome.Equals(outroCliente.Nome) &&
                   this.Idade.Equals(outroCliente.Idade);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}