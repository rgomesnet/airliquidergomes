﻿using System;
using System.Threading.Tasks;
using Domain.Models;

namespace Domain.Services
{
    public interface IObterCliente
    {
        Task<Cliente> Execute(Guid clienteId);
    }
}
