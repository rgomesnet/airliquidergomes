﻿using Domain.Models;
using Domain.Services.DTO;
using System.Threading.Tasks;

namespace Domain.Services
{
    public interface ICriarCliente
    {
        Task<Cliente> Execute(CriarClienteDTO novoCliente);
    }
}