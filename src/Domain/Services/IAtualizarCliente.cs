﻿using Domain.Models;
using Domain.Services.DTO;
using System.Threading.Tasks;

namespace Domain.Services
{
    public interface IAtualizarCliente
    {
        Task<Cliente> Execute(AtualizarClienteDTO clienteDTO);
    }
}
