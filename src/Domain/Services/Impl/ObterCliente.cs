﻿namespace Domain.Services.Impl
{
    using System;
    using System.Threading.Tasks;
    using Domain.Models;
    using Domain.Repositories;

    public class ObterCliente : IObterCliente
    {
        private readonly IClienteRepository repository;
        public ObterCliente(IClienteRepository repository)
            => this.repository = repository;

        public async Task<Cliente> Execute(Guid clienteId)
        {
            var cliente = await this.repository.Obter(clienteId);

            if (cliente == null)
            {
                throw new ApplicationException($"O cliente com id {clienteId} não existe.");
            }

            return cliente;
        }
    }
}
