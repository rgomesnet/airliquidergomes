﻿using Domain.Models;
using Domain.Repositories;
using Domain.Services.DTO;
using System;
using System.Threading.Tasks;

namespace Domain.Services.Impl
{
    public class AtualizarCliente : IAtualizarCliente
    {
        private readonly IClienteRepository repository;
        public AtualizarCliente(IClienteRepository repository)
            => this.repository = repository;

        public async Task<Cliente> Execute(AtualizarClienteDTO clienteDTO)
        {
            var clienteExistente = await this.repository.Obter(clienteDTO.Id);

            if (clienteExistente == null)
            {
                throw new ApplicationException("Não é possível atualizar um cliente inexistente.");
            }

            if (clienteExistente.Equals(clienteDTO.ToModel()))
            {
                throw new ApplicationException("Não há informações a serem atualizados. Nome e idade são os mesmos já cadastrado.");
            }

            clienteExistente.Nome = clienteDTO.Nome;
            clienteExistente.Idade = clienteDTO.Idade;

            return await this.repository.Atualizar(clienteExistente);
        }
    }
}
