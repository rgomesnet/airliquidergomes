﻿namespace Domain.Services.Impl
{
    using System;
    using System.Threading.Tasks;
    using Domain.Models;
    using Domain.Repositories;
    using Domain.Services.DTO;

    public class CriarCliente : ICriarCliente
    {
        private readonly IClienteRepository repository;
        public CriarCliente(IClienteRepository repository)
            => this.repository = repository;

        public async Task<Cliente> Execute(CriarClienteDTO clienteDTO)
        {
            if (string.IsNullOrEmpty(clienteDTO.Nome)
                || clienteDTO.Idade < 0
                || clienteDTO.Idade >= 150)
            {
                throw new ApplicationException($"Dados incorretos para criar um cliente. O nome não pode ser nulo e a idade deve ser menor do 150.");
            }

            return await this.repository.Criar(clienteDTO.ToModel());
        }
    }
}
