﻿namespace Domain.Services.Impl
{
    using System;
    using System.Threading.Tasks;
    using Domain.Repositories;

    public class ExcluirCliente : IExcluirCliente
    {
        private readonly IClienteRepository repository;
        public ExcluirCliente(IClienteRepository repository)
            => this.repository = repository;

        public async Task Execute(Guid clienteId)
        {
            var cliente = await this.repository.Obter(clienteId);

            if (cliente == null)
            {
                throw new ApplicationException("Não é possível excluir um cliente inexistente.");
            }

            await this.repository.Excluir(cliente);
        }
    }
}