﻿using System;
using System.Threading.Tasks;

namespace Domain.Services
{
    public interface IExcluirCliente
    {
        Task Execute(Guid clienteId);
    }
}
