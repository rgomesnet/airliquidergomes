﻿namespace Domain.Services.DTO
{
    public class CriarClienteDTO
    {
        public string Nome { get; set; }
        public int Idade { get; set; }
    }
}
