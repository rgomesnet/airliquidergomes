﻿using Domain.Models;
using Domain.Services.DTO;

namespace Domain.Services
{
    public static class ClienteExtensions
    {
        public static Cliente ToModel(this CriarClienteDTO dto)
        {
            return new Cliente
            {
                Nome = dto.Nome,
                Idade = dto.Idade
            };
        }

        public static Cliente ToModel(this AtualizarClienteDTO dto)
        {
            return new Cliente
            {
                Id = dto.Id,
                Nome = dto.Nome,
                Idade = dto.Idade
            };
        }
    }
}
