﻿using System;

namespace Domain.Services.DTO
{
    public class AtualizarClienteDTO
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public int Idade { get; set; }
    }
}
