﻿using Domain.Models;
using Domain.Services.DTO;
using System;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IClienteRepository
    {
        Task<Cliente> Obter(Guid clienteId);

        Task<Cliente> Criar(Cliente cliente);

        Task<Cliente> Atualizar(Cliente cliente);

        Task Excluir(Cliente cliente);
    }
}
