using Domain.Repositories;
using Domain.Services;
using Domain.Services.Impl;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Repository;
using Repository.Infra;
using System.Threading;
using System.Threading.Tasks;

namespace ClienteAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCorrelationIdHandler();
            services.AddExceptionHandler();

            services.AddDbContext<ClienteContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("Default"));
            });

            services.AddTransient<IClienteRepository, ClienteRepository>();
            services.AddTransient<ICriarCliente, CriarCliente>();
            services.AddTransient<IObterCliente, ObterCliente>();
            services.AddTransient<IAtualizarCliente, AtualizarCliente>();
            services.AddTransient<IExcluirCliente, ExcluirCliente>();

            services.AddControllers();

            services.AddSwaggerGen();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ClienteContext clienteContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Cliente API V1");
            });

            app.UseCorrelationIdHandler();
            app.UseExceptionHandler();

            app.UseRouting();

            app.UseAuthorization();

            clienteContext.Database.Migrate();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
