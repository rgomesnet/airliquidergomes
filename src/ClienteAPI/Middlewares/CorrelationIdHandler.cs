﻿using System;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;

namespace ClienteAPI
{
    public class CorrelationIdHandler : IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            context.Request.Headers.TryGetValue("CorrelationId", out var correlationId);

            if (string.IsNullOrEmpty(correlationId))
            {
                context.Request.Headers.Add("CorrelationId", Guid.NewGuid().ToString());
            }
            await next(context);
        }
    }

    public static class CorrelationIdHandlerExtensions
    {
        public static IServiceCollection AddCorrelationIdHandler(this IServiceCollection services)
        {
            return services.AddTransient<CorrelationIdHandler>();
        }

        public static void UseCorrelationIdHandler(this IApplicationBuilder app)
        {
            app.UseMiddleware<CorrelationIdHandler>();
        }
    }
}
