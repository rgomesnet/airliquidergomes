﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using System;
using System.Threading.Tasks;

namespace ClienteAPI
{
    public class ExceptionHandler : IMiddleware
    {
        private readonly ILogger<ExceptionHandler> logger;
        public ExceptionHandler(ILogger<ExceptionHandler> logger)
        {
            this.logger = logger;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (ApplicationException ex)
            {
                context.Response.StatusCode = 400;
                await context.Response.WriteAsync(ex.Message);
            }
            catch (SqlException ex)
            {
                context.Request.Headers.TryGetValue("CorrelationId", out StringValues correlationId);
                this.logger.LogError(ex, $"Erro no SQL Server. CorrelationId: {correlationId}");
                context.Response.StatusCode = 500;
                await context.Response.WriteAsync($"Não foi possível realizar a operação, tente mais tarde.");
            }
            catch (Exception ex)
            {
                context.Request.Headers.TryGetValue("CorrelationId", out StringValues correlationId);
                this.logger.LogError(ex, $"Erro desconhecido: {correlationId}");

                context.Response.StatusCode = 500;
                context.Response.Headers.Add("CorrelationId", correlationId);
                await context.Response.WriteAsync($"Não foi possível realizar a operação, tente mais tarde.");
            }
        }

        private void AdicionarCorrelationId(HttpContext context)
        {
            context.Request.Headers.TryGetValue("CorrelationId", out StringValues correlationId);
            context.Response.Headers.Add("CorrelationId", correlationId);
        }
    }

    public static class ExceptionHandlerExtensions
    {
        public static IServiceCollection AddExceptionHandler(this IServiceCollection services)
        {
            return services.AddTransient<ExceptionHandler>();
        }

        public static void UseExceptionHandler(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionHandler>();
        }
    }
}
