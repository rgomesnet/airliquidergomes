﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Domain.Services;
using Domain.Models;
using Domain.Services.DTO;

namespace ClienteAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private readonly ICriarCliente criarClienteService;
        private readonly IAtualizarCliente atualizarClienteService;
        private readonly IObterCliente obterClienteService;
        private readonly IExcluirCliente excluirClienteService;

        public ClientesController(ICriarCliente criarClienteService,
                                  IAtualizarCliente atualizarClienteService,
                                  IObterCliente obterClienteService,
                                  IExcluirCliente excluirClienteService)
        {
            this.criarClienteService = criarClienteService;
            this.atualizarClienteService = atualizarClienteService;
            this.obterClienteService = obterClienteService;
            this.excluirClienteService = excluirClienteService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(Guid id)
        {
            var cliente = await this.obterClienteService.Execute(id);
            return Ok(cliente);
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] CriarClienteDTO clienteDTO)
        {
            var cliente = await this.criarClienteService.Execute(clienteDTO);
            return Ok(cliente);
        }

        [HttpPut]
        public async Task<ActionResult> Put([FromBody] AtualizarClienteDTO clienteDTO)
        {
            var clienteAtualizado = await this.atualizarClienteService.Execute(clienteDTO);
            return Ok(clienteAtualizado);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            await this.excluirClienteService.Execute(id);
            return NoContent();
        }
    }
}
